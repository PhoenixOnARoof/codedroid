exports.run = async (guild) => {
    const blacklist = await db.table('blacklist').get('global').pluck('servers').run();
    const blacklisted = blacklist.servers.indexOf(guild.id) !== -1;

    const config = require("../config.json");
    const dbl = new DBL('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjM3MzE5MzAyMDcwMDM2MDcxNCIsImJvdCI6dHJ1ZSwiaWF0IjoxNTIwMTY4MDMxfQ.4GGNeRM-YPjmgPlzr862XOlc7TWMBqgV_DOTYRtfxm8', bot);
    const boats = new DiscordBoats({token: "6pT1pAdDjxFZptfyE2DjSosvDoPz6g"});
    const channel = bot.channels.get("412077189496832020");
    const logmsg = [
        '<:customCheck:406637433569017857> **__Added to a guild!__**',
        `**__Name:__** ${guild.name}`,
        `**__Guild ID:__** ${guild.id}`,
        `**__Region:__** ${guild.region}`,
        `**__Owner:__** ${guild.owner.user.tag} (${guild.owner.user.id})`,
        `**__# of Members:__** ${guild.memberCount}`,
        blacklisted ? '\n**Blacklisted server, leaving**' : ''
    ].join('\n');
    channel.send(logmsg);

    dbl.postStats(bot.guilds.size).then(() => console.log(`Posted Guild count to Discord Bot List. Guild count is now: ${bot.guilds.size}`));
    boats.postGuilds(bot.guilds.size).then(() => console.log(`Posted Guild count to discordboats.club. Guild count is now: ${bot.guilds.size}`));

    bot.user.setActivity(`with ${bot.guilds.size} servers | ${config.prefix}help`);

    if (blacklisted) {
        try {
            await guild.owner.send(`<:customX:406637433677938718> Your server **${guild.name}** has been blacklisted from using this bot, to get your server removed from the blacklist you can join the support server at https://discord.gg/7g2raC4`);
            guild.leave();
        }
        catch(e) {
            guild.leave();
        }
    }
};

exports.run = async member => {
    const guildSettings = await getSettings(member.guild);
  
    if(guildSettings.joinLeaveLog === null) return;
    const channel = member.guild.channels.get(guildSettings.joinLeaveLog);
    let message = guildSettings.joinMessage;
    message = message.replace(/\{\{user\}\}/gi, member.toString());
    message = message.replace(/\{\{server\}\}/gi, member.guild.name);
  
    if(!channel) return;
    
    channel.send(message);
};

exports.run = (oldMem, newMem) => {
    const connection = newMem.guild.voiceConnection;
    if (!connection) return;

    const channel = connection.channel;

    if (channel.members.size < 2) {
        if(!music.connections[channel.guild.id]) return;
        const startChannel = channel.guild.channels.get(music.connections[channel.guild.id].startChannel);
        if (startChannel && music.connections[channel.guild.id] !== null) startChannel.send("Everyone left the voice channel, leaving the channel");

        music.connections[channel.guild.id] = null;
        channel.leave();
    }
};
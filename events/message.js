exports.run = async msg => {
	if (msg.author.bot) return;

  const settings = await getSettings(msg.guild);
	let prefix = settings.prefix;
	let mention = msg.content.match(new RegExp(`(^ *<@!?${bot.user.id}> *).+`));

	if (!msg.content.toLowerCase().startsWith(prefix.toLowerCase()) && !mention) return;
	if (mention) {
		msg.mentions.users = msg.mentions.users.filter(user => user.id !== bot.user.id);
		if (msg.guild) msg.mentions.members = msg.mentions.members.filter(mem => mem.id !== bot.user.id);
	}

	let args = msg.content.slice((mention ? mention[1] : prefix).length).trim().split(/ +/);
	let command = args.shift().toLowerCase();

  if(!bot.commands.hasOwnProperty(command)) return;
  
  const globalBlacklist = await db.table('blacklist').get('global').pluck('users').run();
  if (globalBlacklist.users.indexOf(msg.author.id) !== -1) {
    return msg.channel.send(`<:customX:406637433677938718> ${msg.author} you are blacklisted from using this bot, if you believe this is an error or want to appeal please join the support server at https://discord.gg/7g2raC4`);
  }

  try {
    const cmd = bot.commands[command];
    const otherData = {
      prefix,
      guildSettings: settings
    };
    
    const optionCheck = checkOptions(msg, cmd.options);
    if (optionCheck !== true) return msg.channel.send(optionCheck);
    
    cmd.run(msg, args, otherData);
  }
  catch(e) {
    console.error(e);
    msg.channel.send("<:customX:406637433677938718> An error has occurred while running the command. The error has been logged");
  }
};

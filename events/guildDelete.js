exports.run = (guild) => {

    const config = require("../config.json");
    const dbl = new DBL('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjM3MzE5MzAyMDcwMDM2MDcxNCIsImJvdCI6dHJ1ZSwiaWF0IjoxNTIwMTY4MDMxfQ.4GGNeRM-YPjmgPlzr862XOlc7TWMBqgV_DOTYRtfxm8', bot);
    const boats = new DiscordBoats({token: "6pT1pAdDjxFZptfyE2DjSosvDoPz6g"});
    const channel = bot.channels.get("412077189496832020");
    const logmsg = [
        '<:customWarning:407019356011102218> **__Removed from a guild!__**',
        `**__Name:__** ${guild.name}`,
        `**__Guild ID:__** ${guild.id}`,
        `**__Region:__** ${guild.region}`,
        `**__Owner:__** ${guild.owner.user.tag} (${guild.owner.user.id})`,
        `**__# of Members:__** ${guild.memberCount}`
    ].join('\n');
    channel.send(logmsg);

    dbl.postStats(bot.guilds.size).then(() => console.log(`Posted Guild count to Discord Bot List. Guild count is now: ${bot.guilds.size}`));
    boats.postGuilds(bot.guilds.size).then(() => console.log(`Posted Guild count to discordboats.club. Guild count is now: ${bot.guilds.size}`));

    bot.user.setActivity(`with ${bot.guilds.size} servers | ${config.prefix}help`);
};

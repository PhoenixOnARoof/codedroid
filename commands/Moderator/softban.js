exports.run = (msg, args, data) => {
    const member = msg.mentions.members.first();
    const reason = args.slice(1).join(" ");

    if (!member) return msg.channel.send("<:customX:406637433677938718> Nobody was mentioned to softban");

    if (!member.bannable) return msg.channel.send("<:customX:406637433677938718> That person cannot be softbanned");

    member.ban({ days: 7, reason }).then(() => {
        member.guild.unban(member, reason).then(() => {
            msg.channel.send(`<:customCheck:406637433569017857> ${member} was softbanned for **${reason}**`);
        })
    })
};

exports.help = {
    usage: "<member> [reason]",
    description: "Bans the specified member to remove their messsages and then immediately unbans them"
};

exports.options = {
    userPermissions: ['BAN_MEMBERS'],
    botPermissions: ['BAN_MEMBERS']
};
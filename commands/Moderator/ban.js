exports.run = (msg, args, data) => {
	const member = msg.mentions.members.first();
	const reason = args.slice(1).join(" ");

	if (!member) return msg.channel.send("<:customX:406637433677938718> Nobody was mentioned to ban");

	if (!member.bannable) return msg.channel.send("<:customX:406637433677938718> That person cannot be banned");

	member.ban({ days: 7, reason }).then(() => {
		msg.channel.send(`<:customBanHammer:407371624980152332> ${member} was successfully banned`);
	}).catch(() => {
		msg.channel.send("<:customX:406637433677938718> An unexpected error has occurred while banning the member.");
	});
};

exports.help = {
	usage: "<member> [reason]",
	description: "Bans the mentioned person for the specified reason."
};

exports.options = {
	userPermissions: ['BAN_MEMBERS'],
	botPermissions: ['BAN_MEMBERS']
};
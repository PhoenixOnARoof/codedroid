exports.run = (msg, args, data) => {
    let member = msg.guild.member(msg.mentions.users.first()) || msg.guild.members.get(args[0]);
    if (!member) return msg.channel.send(`<:customX:406637433677938718> Couldn't find that user. Do they actually exist?`)
    let newnick = args.slice(1).join(" ");
    if (!newnick) return msg.channel.send(`<:customX:406637433677938718> Please specifiy a nickname!`);

    member.setNickname(newnick).then(() => {
        msg.channel.send(`<:customCheck:406637433569017857> Changed nickname of <@${member.id}> to **${newnick}**. `)
    }).catch((e) => {
        msg.channel.send(`<:customX:406637433677938718> An unexpected error has occurred while changing the nickname of <@${member.id}>. The error has been logged.`);
        console.log(e);
    });

};

exports.help = {
    usage: "<member> [newnickname]",
    description: "Changes a specified user's nickname to the specified nickname"
};

exports.options = {
	userPermissions: ['MANAGE_NICKNAMES'],
	botPermissions: ['MANAGE_NICKNAMES']
};
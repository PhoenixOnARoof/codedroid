exports.run = (msg, args, data) => {
    let member = msg.guild.member(msg.mentions.users.first()) || msg.guild.members.get(args[0]);
    if (!member) return msg.channel.send(`<:customX:406637433677938718> Couldn't find that user. Do they actually exist?`)
    let role = args.slice(1).join(" ");
    if (!role) return msg.channel.send(`<:customX:406637433677938718> Please specifiy a role!`);
    let gRole = msg.guild.roles.find(`name`, role);
    if (!gRole) return msg.channel.send(`<:customX:406637433677938718> Couldn't find that role. Does that role actually exist?`);

    if (!member.roles.has(gRole.id)) return msg.channel.send(`<:customX:406637433677938718> That user doesn't have that role.`);
    member.removeRole(gRole.id).then(() => {
        msg.channel.send(`<:customCheck:406637433569017857> Removed role **${gRole.name}** from <@${member.id}>. `)
    }).catch((e) => {
        msg.channel.send(`<:customX:406637433677938718> An unexpected error has occurred while removing the role from <@${member.id}>. The error has been logged.`);
        console.log(e);
    });

};

exports.help = {
    usage: "<member> [role (role can have spaces)]",
    description: "Removes the specified role from the specified user"
};

exports.options = {
    userPermissions: ['MANAGE_ROLES'],
    botPermissions: ['MANAGE_ROLES']
};
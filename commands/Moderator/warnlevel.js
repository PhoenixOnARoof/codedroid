exports.run = (msg, args, data) => {
    let warns = JSON.parse(fs.readFileSync("./json/warns.json", "utf8"));
    let user = msg.guild.member(msg.mentions.users.first()) || msg.guild.members.get(args[0])
    if (!user) return msg.channel.send(`<:customX:406637433677938718> Couldn't find that person. Do they actually exist?`);
    if (!warns[user.id]) return msg.channel.send(`<:customScroll:406920316271722496> That person has never been warned.`)
    let warnlevel = warns[user.id].warns

    msg.channel.send(`<:customScroll:406920316271722496> ${user} has **${warnlevel}** warnings.`)

};

exports.help = {
    usage: "<member>",
    description: "Allows a user to check the amount of warnings a person has"
};

exports.options = {
    userPermissions: ['MANAGE_MESSAGES']
};
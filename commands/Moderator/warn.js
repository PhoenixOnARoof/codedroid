exports.run = (msg, args, data) => {
    let warns = JSON.parse(fs.readFileSync("./json/warns.json", "utf8"));
    let user = msg.guild.member(msg.mentions.users.first()) || msg.guild.members.get(args[0])
    if (!user) return msg.channel.send("<:customX:406637433677938718> Couldn't find that person. Do they actually exist?")
    //if (user.hasPermission("MANAGE_MESSAGES")) return msg.channel.send("<:customX:406637433677938718> That person cannot be warned.");
    let reason = args.slice(1).join(" ");

    if (!warns[user.id]) warns[user.id] = {
        warns: 0
    };

    warns[user.id].warns++;

    fs.writeFileSync("./json/warns.json", JSON.stringify(warns));

    msg.channel.send(`<:customScroll:406920316271722496> Warned ${user} for **${reason}**!${warns[user.id].warns >= 3 ? `\n<:customWarning:407019356011102218> ${user} now has 3 or more warnings. A punishment is recommended!` : ''}`);

};

exports.help = {
    usage: "<member> [reason for warn]",
    description: "Warns the specified user for the specified reason"
};

exports.options = {
    userPermissions: ['MANAGE_MESSAGES']
};
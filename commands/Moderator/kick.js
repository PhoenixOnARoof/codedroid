exports.run = (msg, args, data) => {
	let member = msg.mentions.members.first();
	let reason = args.slice(1).join(" ");

	if (!member) return msg.channel.send("<:customX:406637433677938718> Nobody was mention to kick");

	if (!member.kickable) return msg.channel.send("<:customX:406637433677938718> That person cannot be kicked");

	member.kick(reason);
	msg.channel.send(`<:customCheck:406637433569017857> ${member} was kicked for **${reason}**`);
};

exports.help = {
	usage: "<member> [reason]",
	description: "Kicks the mentioned person for the specified reason"
};

exports.options = {
    userPermissions: ['KICK_MEMBERS'],
    BotPermissions: ['KICK_MEMBERS']
};
exports.run = (msg, args, data) => {
	if (!(/^\d+$/.exec(args[0]))) return msg.channel.send("<:customX:406637433677938718> Enter a number");

	const amount = +args[0];
	if (amount < 2 || amount > 100) return msg.channel.send("<:customX:406637433677938718> Your number must be between 2 and 100");

	msg.delete().catch(() => { }).then(() => {
		msg.channel.bulkDelete(amount, true).then(msgs => {
			msg.channel.send(`<:customCheck:406637433569017857> Successfully cleared ${msgs.size} messages!`).then(m => m.delete(5000));
		}).catch(() => {
			msg.channel.send("<:customX:406637433677938718> An unknown error has occured while deleting messages");
		});
	});
}

exports.help = {
	usage: "<# of messages to delete>",
	description: "Removes the specified amount of messages"
};

exports.options = {
	userPermissions: ['MANAGE_MESSAGES'],
	botPermissions: ['MANAGE_MESSAGES']
};
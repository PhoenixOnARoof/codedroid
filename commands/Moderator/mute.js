exports.run = (msg, args, data) => {
    const member = msg.mentions.members.first();
    const reason = args.slice(1).join(" ");

    if (!member) return msg.channel.send("<:customX:406637433677938718> Nobody was mentioned to mute");

    const mutedRole = msg.guild.roles.find("name", "Muted");
    if (!mutedRole) return msg.channel.send("<:customX:406637433677938718> I can't find the `Muted` role");

    if (member.hasPermission("ADMINISTRATOR")) return msg.channel.send(":confounded: I could try muting an admin but it wouldn't do anything!")

    if (!member.roles.has(mutedRole.id)) {
        member.addRole(mutedRole, reason).then(() => {
            msg.channel.send(`<:customCheck:406637433569017857> Successfully muted ${member}!`);
        }).catch(() => {
            msg.channel.send("<:customX:406637433677938718> An unknown error has occured while muting");
        });
    }
    else {
        member.removeRole(mutedRole, reason).then(() => {
            msg.channel.send(`<:customCheck:406637433569017857> Successfully unmuted ${member}!`);
        }).catch(() => {
            msg.channel.send("<:customX:406637433677938718> An unknown error has occured while unmuting");
        });
    }
};

exports.help = {
    usage: "<user> [reason]",
    description: "Mutes the mentioned user"
};

exports.options = {
    userPermissions: ['MANAGE_ROLES'],
    botPermissions: ['MANAGE_ROLES']
};
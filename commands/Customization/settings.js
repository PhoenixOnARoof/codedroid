exports.run = async (msg, args, data) => {
    const settingsConfig = rq('./settingsConfig.js');

    const settingsOverview = () => {
        const embed = {
            title: `${msg.guild.name} Settings`,
            color: 0x36393e,
            description: `For more information, do ${data.prefix}settings help`,
            fields: []
        };

        for (let i in data.guildSettings) {
            if (data.guildSettings.hasOwnProperty(i)) {
                const value = settingsConfig[i].readable(data.guildSettings[i], msg);

                embed.fields.push({
                    name: i,
                    value,
                    inline: true
                });
            }
        }

        msg.channel.send({ embed });
    };

    if (args.length) {
        const subcommand = args[0].toLowerCase();

        if (subcommand === 'help') {
            const embed = {
                title: 'Settings Help',
                color: 0x36393e,
                fields: [
                    {
                        name: 'Listing Available Settings',
                        value: `${data.prefix}settings list`
                    },
                    {
                        name: 'Getting Setting Information',
                        value: `${data.prefix}settings <setting name> help`
                    },
                    {
                        name: 'Setting a Setting',
                        value: `${data.prefix}settings <setting name> set <value or default>`
                    },
                    {
                        name: 'Clearing All Settings',
                        value: `**Server Owner Only** - ${data.prefix}settings reset`
                    }
                ]
            };

            msg.channel.send({ embed });
        }
        else if (subcommand === 'list') {
            msg.channel.send(`The available settings are: ${Object.getOwnPropertyNames(settingsConfig).map(s => `\`${s}\``).join(', ')}`)
        }
        else if (settingsConfig.hasOwnProperty(args[0])) {
            if (!args[1]) return msg.channel.send(`<:customX:406637433677938718> Missing action after the setting name, you can do ${data.prefix}settings help for more info`);
            const subcommand = args[1].toLowerCase();

            const setting = settingsConfig[args[0]];

            if (subcommand === 'help') {
                const embed = {
                    title: `Setting Help - ${args[0]}`,
                    color: 0x3639e,
                    description: setting.help()
                };

                msg.channel.send({ embed });
            }
            else if (subcommand === 'set') {
                const newValue = args.slice(2).join(' ');
                let defaulting = false;

                if (newValue === 'default') defaulting = true;
                else {
                    const validated = setting.set(newValue, msg);
                    
                    if (validated.error) return msg.channel.send(`<:customX:406637433677938718> Error: ${validated.error}`);

                    if (validated.success === setting.default()) defaulting = true;

                    if (!defaulting) {
                        const row = await db.table('serverSettings').get(msg.guild.id).run();
                        if (row) await db.table('serverSettings').get(msg.guild.id).update({ settings: { [args[0]]: validated.success } }).run();
                        else await db.table('serverSettings').insert({ id: msg.guild.id, settings: { [args[0]]: validated.success } }).run();
                        msg.channel.send(`<:customCheck:406637433569017857> ${args[0]} successfully updated to \`${setting.readable(validated.success, msg)}\``);
                    }
                }

                if (defaulting) {
                    await db.table('serverSettings').get(msg.guild.id).replace(db.row.without({ settings: { [args[0]]: true } })).run();

                    msg.channel.send(`<:customCheck:406637433569017857> ${args[0]} successfully updated to \`${setting.readable(setting.default(), msg)}\``);
                }
            }
            else {
                msg.channel.send(`<:customX:406637433677938718> Unknown option \`${args[1]}\`, you can do ${data.prefix}settings help for more info`);
            }
        }
        else if (subcommand === 'reset') {
            if (msg.author.id !== msg.guild.ownerID) return msg.channel.send('<:customLock:406836574571986965> This option is only available to the server owner');

            const m = await msg.channel.send('<:customWarning:407019356011102218> Continuing with this will clear **all** settings for this server and is **not** reversible, to continue, react with <:customCheck:406637433569017857> within 30 seconds');
            await m.react('406637433569017857');

            try {
                const filter = (r, u) => u.id === msg.author.id && r.emoji.id === '406637433569017857';
                const reactions = await m.awaitReactions(filter, { max: 1, time: 30000, errors: ['time'] });
                m.clearReactions();
                await m.edit('Deleting all data...');

                await db.table('serverSettings').get(msg.guild.id).replace(db.row.without('settings')).run();
                await db.table('serverSettings').get(msg.guild.id).update({ settings: {} }).run();

                m.edit('<:customCheck:406637433569017857> Settings successfully deleted!');
            }
            catch(e) {
                m.clearReactions();
                m.edit('<:customX:406637433677938718> Cancelled');
            }
        }
        else {
            msg.channel.send(`<:customX:406637433677938718> Unknown argument/setting \`${args[0]}\`, you can do ${data.prefix}settings help for more info`);
        }

        return;
    }

    settingsOverview();
};

exports.help = {
    usage: "[help]",
	description: "Allows you to configure the bot, do help for more detailed information"
};

exports.options = {
    ownerOverride: true,
    userPermissions: ['MANAGE_GUILD']
};
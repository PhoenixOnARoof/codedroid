const util = require('util');
const cp = require("child_process");

const exec = util.promisify(cp.exec);
const config = rq('./config.json');

exports.run = async (msg, args, data) => {
	const mdContents = ['# CodeDroid Commands'];
	
	for (let i in bot.commandHelp) {
		if (bot.commandHelp.hasOwnProperty(i)) {
			const table = ['| Command | Description |', '| ----- | ----- |'];
			
			bot.commandHelp[i].forEach(c => {
				table.push(`| ${config.prefix}${c.name}${c.usage ? ' ' : ''}${c.usage} | ${c.description} |`);
			});
			
			mdContents.push(`## ${i}`, table.join('\n'));
		}
	}
	
    fs.writeFileSync('./commands.md', mdContents.join('\n').replace(/(<|>)/g, '\\$1'));
	
	try {
		await exec('git add commands.md');
		await exec('git commit -m "Updated commands.md"');
		await exec('git push origin master');
		
		msg.channel.send('commands.md updated on GitLab!');
	}
	catch(e) {
		console.error(e);
		msg.channel.send('Error pushing to git, most likely missing credentials or the commands.md on git is already up to date');
	}
};

exports.help = {
    description: "Generates a commands.md file and pushes to git"
};

exports.options = {
	ownerOnly: true
};
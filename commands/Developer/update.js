const cp = require("child_process");

exports.run = (msg, args, data) => {
    cp.exec("git pull origin master", (err, output) => {
        if (err) return msg.channel.send(`Error: ${err}`);

        msg.channel.send(`Success! \`\`\`\n${output}\`\`\``);
    });
};

exports.help = {
    description: "Pulls the latest code to update the bot"
};

exports.options = {
	ownerOnly: true
};
exports.run = async (msg, args, data) => {
	const type = (args[0] || '').toLowerCase();

    const blacklist = await db.table('blacklist').get('global').run();

    if (type === 'server') {
        const serverId = args[1];
        const server = bot.guilds.get(serverId);
        const index = blacklist.servers.indexOf(serverId);

        if (!server && index === -1) return msg.channel.send('<:customX:406637433677938718> Invalid server id');

        if (index === -1) {
            await db.table('blacklist').get('global').update({ servers: db.row('servers').append(serverId) }).run();
            msg.channel.send(`${server.name} blacklisted`);

            try {
                await server.owner.send(`<:customX:406637433677938718> Your server **${server.name}** has been blacklisted from using this bot, to get your server removed from the blacklist you can join the support server at https://discord.gg/7g2raC4`);
                server.leave();
            }
            catch(e) {
                server.leave();
            }
        }
        else {
            await db.table('blacklist').get('global').update({ servers: db.row('servers').deleteAt(index) }).run();
            msg.channel.send(`${serverId} removed from the blacklist`);
        }
    }
    else if (type === 'user') {
        const userId = (/\d{17,19}/.exec(args[1]) || [])[0];
        const user = bot.users.get(userId);
        const index = blacklist.users.indexOf(userId);

        if (!user && index === -1) return msg.channel.send('<:customX:406637433677938718> Invalid user mention/id');

        if (index === -1) {
            await db.table('blacklist').get('global').update({ users: db.row('users').append(userId) }).run();
            msg.channel.send(`${user.tag} blacklisted`);
        }
        else {
            await db.table('blacklist').get('global').update({ users: db.row('users').deleteAt(index) }).run();
            msg.channel.send(`${(user || { tag: userId }).tag} removed from the blacklist`);
        }
    }
    else if (type === 'list') {
        const embed = {
            title: 'Global Blacklist',
            color: 0x36393e,
            fields: [
                {
                    name: 'Users',
                    value: blacklist.users.length ? blacklist.users.map(u => (bot.users.get(u) || { tag: u }).tag).join(', ') : 'No users blacklisted'
                },
                {
                    name: 'Servers',
                    value: blacklist.servers.length ? blacklist.servers.join(', ') : 'No servers blacklisted'
                }
            ]
        };

        msg.channel.send({ embed });
    }
    else msg.channel.send('<:customX:406637433677938718> Invalid blacklist type');
};

exports.help = {
    usage: '<server or user or list> [mention or id]',
    description: 'Blacklists a server or user globally'
};

exports.options = {
	ownerOnly: true
};
exports.run = (msg, args, data) => {
	msg.channel.send("<:customCheck:406637433569017857> The bot is restarting!").then(() => {
		process.exit(0);
	});
};

exports.help = {
	description: "Restarts the bot"
};

exports.options = {
	ownerOnly: true
};
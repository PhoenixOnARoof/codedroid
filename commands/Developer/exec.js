exports.run = (msg, args, data) => {
    const exec = require('child_process').exec;
    exec(args.join(' '), (err, stdout, stderr) => {
        msg.channel.send(stdout + stderr || '(Empty response)', { code: 'js', split: true });
    });
};

exports.help = {
    usage: "<command>",
    description: "Executes a command on CD's host"
};

exports.options = {
    serverOnly: true,
	ownerOnly: true
};
// Reload all the commands... command :P
exports.run = (msg, args, data) => {
    try {
        loadCommands();
        msg.channel.send("<:customCheck:406637433569017857> Commands reloaded successfully!");
    }
    catch(e) { msg.channel.send("<:customX:406637433677938718> An error has occured!"); console.log(e) }
};

exports.help = {
        description: "Reloads all commands"
};

exports.options = {
    serverOnly: true,
	ownerOnly: true
};
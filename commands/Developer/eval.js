exports.run = (msg, args, data) => {
	const util = require('util');
	const code = args.join(" ");
	let output = null;
	let error = null;

	if (!code) return msg.channel.send('<:customX:406637433677938718> Enter some code to evaluate');

	msg.delete().catch(() => { });

	try {
		output = eval(code);
	} catch (e) {
		error = e;
	}

	if (error) {
		msg.channel.send(`<:customInbox:406930023170310144> Input: \`\`\`js\n${code} \`\`\` <:customOutbox:406930023824752651> <:customX:406637433677938718> Output: \`\`\`js\n${error} \`\`\``);
	}
	else {
		msg.channel.send(`<:customInbox:406930023170310144> Input: \`\`\`js\n${code} \`\`\` <:customOutbox:406930023824752651> <:customCheck:406637433569017857> Output: \`\`\`js\n${util.inspect(output)} \`\`\``);
	}
};

exports.help = {
	usage: "<code>",
	description:   "Evaluates given code"
};

exports.options = {
	serverOnly: true,
	ownerOnly: true
};
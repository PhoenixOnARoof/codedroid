exports.run = (msg, args, data) => {
    if (args.length === 0) return msg.channel.send("<:customX:406637433677938718> Put in a message for me to say");

    const message = args.join(" ");
    msg.delete().catch(() => { });
    msg.channel.send(message);


};
exports.help = {
    description: "Used to say things with the bot"
};

exports.options = {
    serverOnly: true,
	ownerOnly: true
};
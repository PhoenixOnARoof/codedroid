exports.run = (msg, args, data) => {
	const vc = ["None (0)", "Low (1)", "Medium (2)", "(╯°□°）╯︵ ┻━┻ (3)", "┻━┻ ﾐヽ(ಠ益ಠ)ノ彡┻━┻ (4)"][msg.guild.verificationLevel];
	const twofalevel = ["No", "Yes"][msg.guild.mfaLevel];

    const online = m => m.presence.status !== 'offline';
    let members = msg.guild.members.filter(b => !b.user.bot);
    let bots = msg.guild.members.filter(b => b.user.bot);
    let membersOnline = members.filter(online).size;
    let botsOnline = bots.filter(online).size;

	const embed = new Discord.MessageEmbed()
	.setThumbnail(msg.guild.iconURL({ format: 'png' }))
	.setAuthor(`About ${msg.guild.name}`, msg.guild.iconURL({ format: 'png' })) 
	.addField(':id: Guild ID', msg.guild.id, true)
	.addField(':bust_in_silhouette: Owner', msg.guild.owner.user.tag, true)
	.addField(':calendar: Created On', /.+(?= +[A-Z]{3}-\d{4} +\(.+?\))/.exec(msg.guild.createdAt.toString())[0], true)
    .addField(':globe_with_meridians: Region', msg.guild.region, true)
	.addField(':zzz: AFK Timeout', `${msg.guild.afkTimeout} Seconds`, true)
	.addField('<:customCheck:406637433569017857> Verification Level', vc, true)
	.addField(':iphone: 2FA Enabled', twofalevel, true)
	.addField(':busts_in_silhouette: Members', `${members.size} (Online: ${membersOnline})`, true)
	.addField(':robot: Bots', `${bots.size} (Online: ${botsOnline})`, true)
	.addField(':frame_photo: Guild Icon URL', msg.guild.iconURL({ format: 'png', size: 512 }), true)
	.addField('<:customScroll:406920316271722496> More Info', `Text Channels (${msg.guild.channels.filter(c => c.type === "text").size}) - Run ${data.prefix}guildchannels to see the full list.
	Voice Channels (${msg.guild.channels.filter(c => c.type === "voice").size}) - Run ${data.prefix}guildchannels to see the full list.
	Roles (${msg.guild.roles.size}) - Run ${data.prefix}guildroles to see the full list.
	Custom Emojis (${msg.guild.emojis.size}) - Run ${data.prefix}guildemojis to see the full list`)

	msg.channel.send({ embed })

// 	const addspace = ("       ");
//	const serverinfo = [
//		'<:customScroll:406920316271722496> **Basic:**',
//		`${addspace}Name: \`${guild.name}\``,
//		`${addspace}Guild ID: \`${guild.id}\``,
//		`${addspace}Region: \`${guild.region}\``,
//		`${addspace}Channels: ${guild.channels.size} (${guild.channels.filter(c => c.type !== "text" && c.type !== "voice").size} categories, ${guild.channels.filter(c => c.type === "text").size} text, ${guild.channels.filter(c => c.type === "voice").size} voice)`,
//		`${addspace}Owner: \`${guild.owner.user.tag}\``,
//		`${addspace}Members: \`${members}\``,
//		`${addspace}Bots: \`${bots}\``,

//		`<:customPage:412070070571761665> **In-Depth:**`,
//		`${addspace}Roles: ${msg.guild.roles.size}`, //(${msg.guild.roles.map(v => `\`${v.name}\``).join(", ")})
//		`${addspace}Categories: ${guild.channels.filter(c => c.type !== "text" && c.type !== "voice").size}`, //${guild.channels.filter(c => c.type !== "text" && c.type !== "voice").map(c => `\`${c.name}\``).join(", ")}
//		`${addspace}Text Channels: ${guild.channels.filter(c => c.type === "text").size}`, //${guild.channels.filter(c => c.type === "text").map(c => `\`#${c.name}\``).join(", ")}
//		`${addspace}Voice Channels: ${guild.channels.filter(c => c.type === "voice").size}`, //${guild.channels.filter(c => c.type === "voice").map(c => `\`${c.name}\``).join(", ")}
//
//		`<:customHammer:407227418303004673> **Moderation:**`,
//		`${addspace}Verification level: \`${vc}\``
//	].join('\n');
//	msg.channel.send(serverinfo);

};

exports.help = {
	description: "Shows you some info about the server the command is ran in"
};

exports.options = {
	serverOnly: true
};
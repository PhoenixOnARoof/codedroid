exports.run = async (msg, args, data) => {

    weather.find({ search: args.join(" "), degreeType: 'C' }, function (err, result) {
        if (err) msg.channel.send(err);

        if (result === undefined || result.length === 0) {
            msg.channel.send('<:customX:406637433677938718> Please enter a valid location')
            return;
        }

        // Variables
        var current = result[0].current;
        var location = result[0].location;

        // Embed
        const embed = new Discord.RichEmbed()
            .setDescription(`**${current.skytext}**`)
            .setAuthor(`Weather for ${current.observationpoint}`)
            .setThumbnail(current.imageUrl)
            .setColor(0x000B80)
            .addField('Timezone', `UTC${location.timezone}`, true)
            .addField('Degree Type', location.degreetype, true)
            .addField('Temperature', `${current.temperature} Degree${current.temperature === '1' ? '' : 's'}`, true)
            .addField('Feels Like', `${current.feelslike} Degree${current.feelslike === '1' ? '' : 's'}`, true)
            .addField('Winds', current.winddisplay, true)
            .addField('Humidity', `${current.humidity}%`, true)

        msg.channel.send({ embed });
    });
}



exports.help = {
    usage: "<location>",
    description: "Shows weather for the specified place"
};
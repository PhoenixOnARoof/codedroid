exports.run = (msg, args, data) => {
	//const embed = new Discord.RichEmbed()
	//embed.setColor(0x00008B)
	//embed.setFooter(`${data.prefix}stats | CodeDroid`)
	//embed.addField("Creator", `Anthony#3912`, true, true)
	//embed.addField("Guilds", bot.guilds.size, true)
	//embed.addField("Members", bot.guilds.map(g => g.memberCount).reduce((f, l) => f + l), true)
	//embed.addField("Prefix", data.prefix, true)
	//embed.addField("D.js Version", Discord.version, true)
	//embed.addField("NodeJS", process.version, true)
	//embed.addField("Channels", bot.channels.size, true)
	//embed.addField("Tag", bot.user.tag, true)
	//embed.addField("ID", bot.user.id, true)
	//msg.channel.send({ embed });

	const date = new Date(bot.uptime);
	const stats = [
		`**Creator** - \`Anthony#3912\``,
		`**Developers** - \`Anthony#3912 & sdf#6313\``,
		`**# of Guilds** - \`${bot.guilds.size}\``,
		`**# of Channels** - \`${bot.channels.size}\``,
		`**# of Members** - \`${bot.guilds.map(g => g.memberCount).reduce((f, l) => f + l)}\``,
		`**Prefix** - \`${data.prefix}\``,
		`**D.js Version** - \`${Discord.version}\``,
		`**NodeJS Version** - \`${process.version}\``,
		`**Uptime** - \`${date.getUTCFullYear() - 1970}y ${date.getUTCMonth()}m ${date.getUTCDate() - 1}d ${date.getUTCHours()}h ${date.getUTCMinutes()}m ${date.getUTCSeconds()}s\``
	].join('\n');
	msg.channel.send(stats);
};

exports.help = {
	description: "Gives certain stats about the bot"
};
exports.run = (msg, args, data) => {
	const sendHelp = smart => {
		const embed = {
			title: 'CodeDroid Help',
			description: '<:customInformation:407016355343106068> The bot\'s commands can also be found at: https://bit.ly/codedroidcmds',
			color: 0x36393e,
			fields: []
		};
		
		for (let i in bot.commandHelp) {
			if (bot.commandHelp.hasOwnProperty(i)) {
				const field = {
					name: i,
					value: []
				};
				
				const commands = bot.commandHelp[i].filter(c => checkOptions(msg, c.options) === true || !smart);
				if (!commands.length) continue;
				
				commands.forEach(c => {
					field.value.push(`**${data.prefix}${c.name}${c.usage ? ' ' : ''}${c.usage}** - ${c.description}`);
				});
				
				field.value = field.value.join('\n');
				
				embed.fields.push(field);
			}
		}
		
		msg.author.send({ embed }).then(() => {
			if (msg.guild) msg.channel.send(`<:customMail:406844611194257408> Help has been sent ${msg.author}!`);
		}).catch(() => {
			if (msg.guild) if (msg.guild) msg.channel.send("<:customX:406637433677938718> Error sending help, I am either blocked or you have DMs disabled for this server. If you don't want to enable DMs you can get a list of our commands here https://bit.ly/codedroidcmds");
		});
	};

	if (args.length) {
		const subcommand = args.join(' ');
		const lSubcommand = subcommand.toLowerCase();

		if (lSubcommand === 'all') sendHelp(false);
		else if (lSubcommand === 'list') msg.channel.send(`The available categories for help are: ${Object.getOwnPropertyNames(bot.commandHelp).map(k => `\`${k}\``).join(', ')}`);
		else {
			const categories = Object.getOwnPropertyNames(bot.commandHelp);
			const lCategories = categories.map(c => c.toLowerCase());
			const index = lCategories.indexOf(lSubcommand);

			if (index === -1) return msg.channel.send(`<:customX:406637433677938718> Invalid category name \`${subcommand}\``);

			const category = categories[index];

			const embed = {
				title: `CodeDroid Help - ${category}`,
				description: [],
				color: 0x36393e
			};

			bot.commandHelp[category].forEach(c => {
				embed.description.push(`**${data.prefix}${c.name}${c.usage ? ' ' : ''}${c.usage}** - ${c.description}`);
			});
			
			embed.description = embed.description.join('\n');
			
			msg.channel.send({ embed });
		}

		return;
	}
	
	sendHelp(true);
};

exports.help = {
	usage: '[all or list or category name]',
    description: "Sends the bot's commands that you can use to you, all will send all commands, list will list the available categories, and a specific category will show all commands in that category"
};

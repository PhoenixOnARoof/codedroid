exports.run = (msg, args, data) => {
	if (args.length === 0) {
		msg.channel.send("<:customX:406637433677938718> You didn't provide a message to send!");
	} else {
		msg.delete().catch(() => { });
		const bug = args.join(" ");
		const bugChannel = bot.channels.get('373246363523088395')
		msg.channel.send("<:customCheck:406637433569017857> Thanks! Your bug report has been sent!").then(m => {
			m.delete(5000);
		});
		bugChannel.send(`**Bug Report:**\n\nServer: ${msg.guild ? msg.guild.name : "DMs"}\nRequester: ${msg.author.tag}\nMessage: ${bug}`);
	}
};

exports.help = {
	usage: "<bug>",
	description: "Reports a bug to the developer(s)"
};

exports.options = {
	serverOnly: true
};
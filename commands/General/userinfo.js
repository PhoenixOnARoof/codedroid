exports.run = (msg, args, data) => {

    let member = msg.mentions.members.first();
    if (!member) return msg.channel.send("<:customX:406637433677938718> Nobody was mentioned to get user info for.");

    const embed = new Discord.MessageEmbed()
    .setThumbnail(member.user.avatarURL({ format: 'png' }))
    .setAuthor(`About ${member.user.tag}`, member.user.avatarURL({ format: 'png' }))
    .addField('Tag', `<@${member.user.id}>`, true)
    .addField('User ID', member.user.id, true)
    .addField('Created On', /.+(?= +[A-Z]{3}-\d{4} +\(.+?\))/.exec(member.user.createdAt.toString())[0], true)
    .addField('Nickname', member.nickname, true)

    msg.channel.send({ embed })
};

exports.help = {
	description: "Shows you some info about the mentioned user."
};

exports.options = {
	serverOnly: true
};
exports.run = (msg, args, data) => {
	if (args.length === 0) {
		msg.channel.send("<:customX:406637433677938718> You didn't provide a message to send!");
	} else {
		msg.delete().catch(() => { });
		const request = args.join(" ");
		const requestChannel = bot.channels.get('373941954280685579');
		msg.channel.send("<:customCheck:406637433569017857> Thanks! Your feature request has been sent!").then(m => {
			m.delete(5000);
		});
		requestChannel.send(`**Feature Request:**\n\nServer: ${msg.guild ? msg.guild.name : "DMs"}\nRequester: ${msg.author.tag}\nMessage: ${request}`);
	}
};

exports.help = {
	usage: "<feature>",
	description: "Sends a feature request to the developer(s)."
};

exports.options = {
	serverOnly: true
};
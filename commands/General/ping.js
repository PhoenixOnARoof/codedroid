exports.run = async (msg, args, data) => {
	//let myArray = [':ballot_box_with_check: Ping? What? What is this ping nonsense you speak of?', ':ballot_box_with_check: Ping.. ping... pi- what?', ':ballot_box_with_check: PONG! I wonder how much this is used :thinking:'];
	//let myString = myArray[Math.floor(Math.random() * myArray.length)];
	//embed.addField("Pong! Result is " + Math.round(bot.ws.ping) + "ms", myString, true)

	//msg.channel.send("<:customCheck:406637433569017857> Pong! Result is " + Math.round(bot.ws.ping) + "ms");

	const m = await msg.channel.send("Pong!");
	m.edit(`<:customCheck:406637433569017857> Pong! ${m.createdTimestamp - msg.createdTimestamp}ms response, ${Math.round(bot.ws.ping)}ms API heartbeat`);
};


exports.help = {
	description: "Pings the bot, and returns the pong time"
};
exports.run = (msg, args, data) => {
    const { get } = require("snekfetch");

    get('https://dog.ceo/api/breeds/image/random').then(res => {
        msg.channel.send({ file: res.body.message });
    });

};

exports.help = {
    description: "Gets a random dog image for you to enjoy"
};

exports.options = {
    serverOnly: true
};
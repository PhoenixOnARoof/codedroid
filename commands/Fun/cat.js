exports.run = (msg, args, data) => {
    const { get } = require("snekfetch");

    get('http://thecatapi.com/api/images/get?type=png&size=full').then(res => {
        return msg.channel.send({ files: [{ attachment: res.body }] });
    });

};

exports.help = {
    description: "Gets a random cat image for you to enjoy"
};

exports.options = {
    serverOnly: true
};
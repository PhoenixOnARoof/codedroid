const binary = {
	to: s => s.split('').map(c => c.charCodeAt(0).toString(2).padStart(8, '0')).join(' '),
	from: b => b.split(/ +/g).map(c => String.fromCharCode(parseInt(c, 2))).join('')
};

exports.run = (msg, args, data) => {
    const subcommand = (args[0] || '').toLowerCase();
    const toConvert = args.slice(1).join(' ');
    if (['to', 'from'].indexOf(subcommand) === -1) return msg.channel.send('<:customX:406637433677938718> You must either choose `to` or `from`');
    if (!toConvert) return msg.channel.send('<:customX:406637433677938718> You must enter text or binary to convert');
    
    if (subcommand === 'to') {
        let result = `Your binary is:\n\`\`\`${binary.to(toConvert)}\`\`\``;
        if (result.length > 2000) result = '<:customX:406637433677938718> Binary result too long, try entering something shorter';
        
        msg.channel.send(result);
    }
    else if (subcommand === 'from') {
        if (!/^([01]{8} +)*[01]{8}$/.exec(toConvert)) return msg.channel.send('<:customX:406637433677938718> The given binary does not match the right format');

        msg.channel.send(`Your text is:\n\`\`\`${binary.from(toConvert)}\`\`\``);
    }
};

exports.help = {
    usage: '<to or from> <text or binary>',
    description: 'Converts to or from binary'
};
exports.run = (msg, args, data) => {
    if (!msg.guild.voiceConnection) return msg.channel.send('<:customX:406637433677938718> I am not in a voice channel right now');
    if (msg.member.voiceChannelID !== msg.guild.voiceConnection.channel.id) return msg.channel.send('<:customX:406637433677938718> You must be in the same voice channel as me to pause the music');
    if (music.connections[msg.guild.id].queue.length === 0) return msg.channel.send('<:customX:406637433677938718> There is nothing playing right now');
    if (music.connections[msg.guild.id].paused) return msg.channel.send('<:customX:406637433677938718> The music is already paused');

    music.connections[msg.guild.id].paused = true;
    music.connections[msg.guild.id].dispatcher.pause();
    msg.channel.send("<:customCheck:406637433569017857> Paused");
};

exports.help = {
    description: "Pauses the music"
};

exports.options = {
    serverOnly: true
};
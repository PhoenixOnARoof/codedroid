exports.run = (msg, args, data) => {
    if (!msg.guild.voiceConnection) return msg.channel.send('<:customX:406637433677938718> I am not in a voice channel right now');
    if (msg.member.voiceChannelID !== msg.guild.voiceConnection.channel.id) return msg.channel.send('<:customX:406637433677938718> You must be in the same voice channel as me to view the queue');
    if (music.connections[msg.guild.id].queue.length === 0) return msg.channel.send('<:customX:406637433677938718> There is nothing playing right now');

    const queue = music.connections[msg.guild.id].queue;
    const timePlaying = new Date(Date.now() - music.connections[msg.guild.id].timeStarted);
    const formattedTime = `${("00" + timePlaying.getUTCHours().toString()).slice(-2)}:${("00" + timePlaying.getUTCMinutes().toString()).slice(-2)}:${("00" + timePlaying.getUTCSeconds().toString()).slice(-2)}`;

    msg.channel.send(`Queue: \`\`\`\nNow Playing: ${queue[0].title} (${formattedTime}/${queue[0].duration})\n\n${queue.slice(1).map((s, i) => `${i + 1}. ${s.title} (${s.duration})`).join("\n")}\`\`\``);
};

exports.help = {
    description: "Shows the current music queue"
};

exports.options = {
    serverOnly: true
};
exports.run = (msg, args, data) => {
    if (!msg.guild.voiceConnection) return msg.channel.send('<:customX:406637433677938718> I am not in a voice channel right now');
    if (msg.member.voiceChannelID !== msg.guild.voiceConnection.channel.id) return msg.channel.send('<:customX:406637433677938718> You must be in the same voice channel as me to view the queue');
    if (music.connections[msg.guild.id].queue.length === 0) return msg.channel.send('<:customX:406637433677938718> There is nothing playing right now');
    if (music.connections[msg.guild.id].queue.length === 1) return msg.channel.send('<:customX:406637433677938718> There is nothing to skip to right now');

    const guildMusic = music.connections[msg.guild.id];
    const memberCount = guildMusic.connection.channel.members.filter(m => !m.user.bot).size;

    if (msg.member.hasPermission("ADMINISTRATOR")) {
        msg.channel.send('<:customCheck:406637433569017857> Skipping...');
        return guildMusic.dispatcher.end();
    }

    if (memberCount === 1) {
        msg.channel.send('<:customCheck:406637433569017857> Skipping...');
        return guildMusic.dispatcher.end();
    }
    guildMusic.skipVotes++;

    if (memberCount === 2) {
        if (guildMusic.skipVotes === 2) {
            msg.channel.send('<:customCheck:406637433569017857> Skipping...');
            return guildMusic.dispatcher.end();
        }
        else {
            msg.channel.send(`<:customCheck:406637433569017857> Recieved ${guildMusic.skipVotes}/2 votes to skip`);
        }
    }
    else if (memberCount === 3) {
        if (guildMusic.skipVotes === 3) {
            msg.channel.send('<:customCheck:406637433569017857> Skipping...');
            guildMusic.dispatcher.end();
        }
        else {
            msg.channel.send(`<:customCheck:406637433569017857> Recieved ${guildMusic.skipVotes}/3 votes to skip`);
        }
    }
    else if (memberCount > 3) {
        const required = Math.ceil(memberCount / 2);

        if (guildMusic.skipVotes === required) {
            msg.channel.send('<:customCheck:406637433569017857> Skipping...');
            guildMusic.dispatcher.end();
        }
        else {
            msg.channel.send(`<:customCheck:406637433569017857> Recieved ${guildMusic.skipVotes}/${required} votes to skip`);
        }
    }
};

exports.help = {
    description: "Vote to skip the currently playing (Auto skips if 1 person is in the channel)"
};

exports.options = {
    serverOnly: true
};
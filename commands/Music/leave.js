exports.run = (msg, args, data) => {
    if (!msg.guild.voiceConnection) return msg.channel.send('<:customX:406637433677938718> I am not in a voice channel right now');
    if (msg.member.voiceChannelID !== msg.guild.voiceConnection.channel.id) return msg.channel.send('<:customX:406637433677938718> You must be in the same voice channel as me to leave');

    music.connections[msg.guild.id] = null;

    msg.guild.voiceConnection.channel.leave();
    msg.channel.send('<:customCheck:406637433569017857> Successfully left!');
};

exports.help = {
    description: "Stops playing music and disconnects from the voice channel"
};

exports.options = {
    serverOnly: true
};
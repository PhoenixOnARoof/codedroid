exports.run = (msg, args, data) => {
    msg.channel.send('<:customWarning:407019356011102218> Music is not fully functional. Things may be broken.');

    if (!msg.member.voiceChannel) return msg.channel.send('<:customX:406637433677938718> You need to be in a voice channel for me to join');
    if (music.connections[msg.guild.id] && music.connections[msg.guild.id].connection.channel.id !== msg.member.voiceChannel.id) return msg.channel.send('<:customX:406637433677938718> I\'m already connected to another channel somewhere else in this server');

    msg.member.voiceChannel.join().then(connection => {
        music.connections[msg.guild.id] = {
            connection: connection,
            startChannel: msg.channel.id,
            queue: [],
            dispatcher: null,
            timeStarted: Date.now(),
            paused: false,
            skipVotes: 0
        };

        if (!msg.guild.voiceConnection) return msg.channel.send('<:customX:406637433677938718> I am not in a voice channel right now');
        if (msg.member.voiceChannelID !== msg.guild.voiceConnection.channel.id) return msg.channel.send('<:customX:406637433677938718> You must be in the same voice channel as me to play music');

        if (!args.length) return msg.channel.send('<:customX:406637433677938718> Enter something to search for or a youtube link');
        const searchQ = args.join(" ");

        const queueSong = songId => {
            youtubeInfo(songId).then(info => {
                const publishedSplit = info.datePublished.split('-');
                const duration = `${("00" + (Math.floor((info.duration / 60) / 60)).toString()).slice(-2)}:${("00" + (Math.floor((info.duration / 60) % 60)).toString()).slice(-2)}:${("00" + (info.duration % 60).toString()).slice(-2)}`;

                msg.channel.send(`Adding \`${info.title}\` to queue. Info:\nURL: ${info.url}\nChannel: ${info.owner}\nPublished: ${publishedSplit[1]}-${publishedSplit[2]}-${publishedSplit[0]}\nViews: ${info.views}\nDuration: ${duration}`);

                music.connections[msg.guild.id].queue.push({
                    title: info.title,
                    duration,
                    url: info.url
                });

                if (music.connections[msg.guild.id].queue.length === 1) {
                    const guildMusic = music.connections[msg.guild.id];

                    const playSong = () => {
                        guildMusic.timeStarted = Date.now();

                        const startChannel = msg.guild.channels.get(guildMusic.startChannel);
                        if (startChannel) startChannel.send(`Starting: ${guildMusic.queue[0].title}`);

                        guildMusic.dispatcher = guildMusic.connection.play(ytdl(guildMusic.queue[0].url, { filter: 'audioonly', quality: 'highestaudio' }));
                        guildMusic.dispatcher.on('end', () => {
                            guildMusic.queue.shift();
                            guildMusic.skipVotes = 0;

                            if (guildMusic.queue.length === 0) {
                                const startChannel = msg.guild.channels.get(guildMusic.startChannel);
                                if (startChannel && music.connections[msg.guild.id] !== null) startChannel.send(`Queue concluded. Add more music with **${data.prefix}play**!`);

                                return
                            }
                            playSong();
                        });
                    };
                    playSong();
                }
            }).catch((e) => {
                msg.channel.send('<:customX:406637433677938718> Error getting video info')
                console.log(e);
            });
        };

        const youtubeURL = /^https?:\/\/(?:www.)?youtube.com\/watch?v=(.+)$/.exec(args[0]);

        if (!youtubeURL) {
            snekfetch.get(`https://www.googleapis.com/youtube/v3/search?part=id&type=video&q=${encodeURIComponent(searchQ)}&key=${music.apiKey}`).then(res => {
                const results = res.body.items;
                if (!results.length) return msg.channel.send('<:customX:406637433677938718> No results found!');

                const songId = results[0].id.videoId;

                queueSong(songId);
            }).catch(() => {
                msg.channel.send('<:customX:406637433677938718> Error searching for video');
            });
        }
        else {
            queueSong(youtubeURL[1]);
        }
    }).catch(() => {
        msg.channel.send('<:customX:406637433677938718> Error playing music!');
    });
};

exports.help = {
    usage: "<song name or youtube url>",
    description: "Allows you to play music by searching for a youtube video or playing a youtube url"
};

exports.options = {
    serverOnly: true
};
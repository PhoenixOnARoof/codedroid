exports.run = (msg, args, data) => {
    if (!msg.guild.voiceConnection) return msg.channel.send('<:customX:406637433677938718> I am not in a voice channel right now');
    if (msg.member.voiceChannelID !== msg.guild.voiceConnection.channel.id) return msg.channel.send('<:customX:406637433677938718> You must be in the same voice channel as me to view the queue');
    if (music.connections[msg.guild.id].queue.length === 0) return msg.channel.send('<:customX:406637433677938718> There is nothing playing right now');

    music.connections[msg.guild.id].paused = false;
    music.connections[msg.guild.id].dispatcher.resume();
    msg.channel.send("<:customCheck:406637433569017857> Resuming...");
};

exports.help = {
    description: "Continue music"
};

exports.options = {
    serverOnly: true
};
// This is the main bot file.
const rethinkdb = require('rethinkdbdash');

global.Discord = require("discord.js");
global.bot = new Discord.Client({ disableEveryone: true });
global.DBL = require("dblapi.js");
global.DiscordBoats = require("dboats-api");
global.fs = require("fs");
global.weather = require('weather-js');
global.db = rethinkdb({ port: 28016, db: 'codedroid' })
global.thisbotisbetterthanminime = true

global.ytdl = require('ytdl-core');
global.youtubeInfo = require('youtube-info');
global.snekfetch = require('snekfetch');
global.http = require('http');

const config = require("./config.json");

global.music = {
	connections: {},
	apiKey: config.ytApiKey,
};

global.rq = f => {
	const path = require.resolve(f);
	const cache = require.cache[path];
	delete require.cache[path];

	if (cache) {
		const cIndex = cache.parent.children.indexOf(cache);
		if (cIndex !== -1) cache.parent.children.splice(cIndex, 1);
	}

	return require(f);
};

global.getSettings = async guild => {
	const settingsConfig = rq('./settingsConfig.js');
	const defaults = {};

	for (let i in settingsConfig) {
		if (settingsConfig.hasOwnProperty(i)) {
			defaults[i] = settingsConfig[i].default();
		}
	}

	if (!guild) return defaults;

	const guildRow = await db.table('serverSettings').get(guild.id).run();
	if (!guildRow) return defaults;

	return Object.assign(defaults, guildRow.settings);
};

global.loadCommands = () => {
  bot.commands = {};
  bot.commandHelp = {};
  
  const commands = fs.readdirSync("./commands");
  commands.forEach(cat => {
    const catCommands = fs.readdirSync(`./commands/${cat}`);

    catCommands.filter(c => c.endsWith(".js")).forEach(c => {
      const cName = c.slice(0, -3);
      
      const commandPath = `./commands/${cat}/${c}`;
      const command = rq(commandPath);
      command.help = command.help || {};
      command.options = command.options || {};

      const commandObject = {
        run: command.run,
        name: cName,
        category: cat,
        help: {
          usage: command.help.usage || "",
          description: command.help.description || "No description"
        },
				options: {
					ownerOverride: command.options.ownerOverride || false,
					ownerOnly: command.options.ownerOnly || false,
					serverOnly: command.options.serverOnly || false,
					userPermissions: command.options.userPermissions || [],
					botPermissions: command.options.botPermissions || []
				}
      };
	  
	  if (!bot.commandHelp.hasOwnProperty(commandObject.category)) bot.commandHelp[commandObject.category] = [];
	  bot.commandHelp[commandObject.category].push(Object.assign({ name: commandObject.name, options: commandObject.options }, commandObject.help));

      bot.commands[cName.toLowerCase()] = commandObject;
    });
  });
};

loadCommands();
console.log("Commands loaded successfully!");

global.checkOptions = (msg, options) => {
	const config = JSON.parse(fs.readFileSync("./config.json"));
	const owner = config.owners.indexOf(msg.author.id) !== -1;

	if (options.ownerOverride && owner) return true;

	if (options.ownerOnly && !owner) return '<:customLock:406836574571986965> Only my developer(s) can use this command.';

	if (!msg.guild && (options.serverOnly || options.userPermissions.length || options.botPermissions.length)) return '<:customX:406637433677938718> This command can only be used in a server';
	
	if (msg.guild) {
		const missingUserPerms = msg.member.permissions.missing(options.userPermissions);
		if (missingUserPerms.length > 0) return `<:customX:406637433677938718> You are missing permission${missingUserPerms.length === 1 ? '' : 's'} ${missingUserPerms.map(p => `\`${p}\``).join(', ')}`;

		const missingBotPerms = msg.guild.me.permissions.missing(options.botPermissions);
		if (missingBotPerms.length > 0) return `<:customX:406637433677938718> I am missing permission${missingBotPerms.length === 1 ? '' : 's'} ${missingBotPerms.map(p => `\`${p}\``).join(', ')}`;
	}

	return true;
};

let oldEmit = bot.emit.bind(bot);
bot.emit = (eventName, ...args) => {
	let events = fs.readdirSync("./events").filter(e => e.endsWith(".js")).map(e => e.slice(0, -3));
	let lowerCaseEvents = events.map(e => e.toLowerCase());
	let index = lowerCaseEvents.indexOf(eventName.toLowerCase());

	if (index !== -1) {
		let eName = `./events/${events[index]}.js`;
		let eventModule = rq(eName);

		try {
			eventModule.run(...args);
		}
		catch (e) { console.log(e); }
	}

	oldEmit(eventName, ...args);
};

process.on('unhandledRejection', console.error);

bot.login(config.token);
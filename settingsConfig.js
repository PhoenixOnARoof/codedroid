module.exports = {
    prefix: {
        set: (value, msg) => {
            if (value.length > 10) return { error: 'the prefix cannot be over 10 characters' };

            return { success: value };
        },
        default: () => require('./config.json').prefix,
        readable: (value, msg) => value,
        help: () => 'Sets the prefix of the server, a maximum of 10 characters'
    },
    joinLeaveLog: {
        set: (value, msg) => {
            if (value === "off") return { success: null };

            const channel = msg.mentions.channels.first();
            if (!channel) return { error: 'the value must be a channel mention or `off`' };

            return { success: channel.id };
        },
        default: () => null,
        readable: (value, msg) => {
            if (!value) return 'off';

            const channel = msg.guild.channels.get(value);
            if (!channel) return 'off';

            return `#${channel.name}`;
        },
        help: () => 'Sets the channel to log join and leave messages, can either be `off` or a channel mention'
    },
    joinMessage: {
        set: (value, msg) => ({ success: value }),
        default: () => "Welcome to {{server}}, {{user}}!",
        readable: (value, msg) => value,
        help: () => 'Sets the message sent to the join/leave log when a user joins, you can use the placeholders `{{server}}` or `{{user}}` to get either the server name, or a mention to the user that joined'
    },
    leaveMessage: {
        set: (value, msg) => ({ success: value }),
        default: () => "{{user}} has left the server",
        readable: (value, msg) => value,
        help: () => 'Sets the message sent to the join/leave log when a user leaves, you can use the placeholders `{{server}}` or `{{user}}` to get either the server name, or a mention to the user that joined'
    }
};
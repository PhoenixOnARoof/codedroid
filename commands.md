# CodeDroid Commands
## Customization
| Command | Description |
| ----- | ----- |
| c/settings [help] | Allows you to configure the bot, do help for more detailed information |
## Developer
| Command | Description |
| ----- | ----- |
| c/eval \<code\> | Evaluates given code |
| c/exec \<command\> | Executes a command on CD's host |
| c/globalBlacklist \<server or user or list\> [mention or id] | Blacklists a server or user globally |
| c/helpMd | Generates a commands.md file and pushes to git |
| c/reload | Reloads all commands |
| c/restart | Restarts the bot |
| c/say | Used to say things with the bot |
| c/update | Pulls the latest code to update the bot |
## Fun
| Command | Description |
| ----- | ----- |
| c/binary \<to or from\> \<text or binary\> | Converts to or from binary |
| c/cat | Gets a random cat image for you to enjoy |
| c/dog | Gets a random dog image for you to enjoy |
## General
| Command | Description |
| ----- | ----- |
| c/frq \<feature\> | Sends a feature request to the developer(s). |
| c/help [all or list or category name] | Sends the bot's commands that you can use to you, all will send all commands, list will list the available categories, and a specific category will show all commands in that category |
| c/invite | Gives you a link to invite the bot to your server |
| c/ping | Pings the bot, and returns the pong time |
| c/report \<bug\> | Reports a bug to the developer(s) |
| c/serverinfo | Shows you some info about the server the command is ran in |
| c/stats | Gives certain stats about the bot |
| c/support | Gives you a link to join the development discord |
| c/userinfo | Shows you some info about the mentioned user. |
| c/weather \<location\> | Shows weather for the specified place |
## Moderator
| Command | Description |
| ----- | ----- |
| c/ban \<member\> [reason] | Bans the mentioned person for the specified reason. |
| c/chngnick \<member\> [newnickname] | Changes a specified user's nickname to the specified nickname |
| c/clear \<# of messages to delete\> | Removes the specified amount of messages |
| c/giverole \<member\> [role (role can have spaces)] | Adds the specified role from the specified user |
| c/kick \<member\> [reason] | Kicks the mentioned person for the specified reason |
| c/mute \<user\> [reason] | Mutes the mentioned user |
| c/removerole \<member\> [role (role can have spaces)] | Removes the specified role from the specified user |
| c/softban \<member\> [reason] | Bans the specified member to remove their messsages and then immediately unbans them |
| c/warn \<member\> [reason for warn] | Warns the specified user for the specified reason |
| c/warnlevel \<member\> | Allows a user to check the amount of warnings a person has |
## Music
| Command | Description |
| ----- | ----- |
| c/leave | Stops playing music and disconnects from the voice channel |
| c/pause | Pauses the music |
| c/play \<song name or youtube url\> | Allows you to play music by searching for a youtube video or playing a youtube url |
| c/queue | Shows the current music queue |
| c/resume | Continue music |
| c/skip | Vote to skip the currently playing (Auto skips if 1 person is in the channel) |